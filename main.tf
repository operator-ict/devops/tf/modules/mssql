resource "azurerm_resource_group" "rg" {
  name     = "rg-mssql-${var.name}"
  location = var.location
  tags     = var.tags
}

locals {
  kv-admin-pass-key = "mssql-admin-password"
}

module "key_vault" {
  source                = "gitlab.com/operator-ict/key-vault/azurerm"
  version               = "0.0.2"
  name                  = "mssql-${var.name}"
  resource_group_name   = azurerm_resource_group.rg.name
  location              = var.location
  expected_kv_passwords = [local.kv-admin-pass-key]
  tags                  = var.tags
}

resource "azurerm_mssql_server" "mssql" {
  name                          = "mssql-${var.name}"
  resource_group_name           = azurerm_resource_group.rg.name
  location                      = azurerm_resource_group.rg.location
  version                       = var.mssql_version
  administrator_login           = "spravce"
  administrator_login_password  = module.key_vault.expected_secrets[local.kv-admin-pass-key].value
  public_network_access_enabled = var.public_network_access

  dynamic "azuread_administrator" {
    for_each = var.azuread_administrator == null ? [] : ["X"]
    content {
      login_username = var.azuread_administrator.login_username
      object_id      = var.azuread_administrator.object_id
    }
  }
  tags                          = var.tags
}

resource "azurerm_mssql_elasticpool" "epool" {
  name                = "epool_${var.name}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  server_name         = azurerm_mssql_server.mssql.name
  license_type        = "LicenseIncluded"
  max_size_gb         = var.max_size_gb
  tags                = var.tags

  sku {
    name     = var.sku.name
    tier     = var.sku.tier
    family   = var.sku.family
    capacity = var.sku.capacity
  }

  per_database_settings {
    min_capacity = var.per_database.min_capacity
    max_capacity = var.per_database.max_capacity
  }
}

resource "azurerm_private_endpoint" "plink" {
  count     = length(var.subnetId) > 0 ? 1 : 0
  name                = "pl-${var.name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  subnet_id           = var.subnetId
  tags                = var.tags

  private_service_connection {
    name                           = "sqlprivatelink"
    private_connection_resource_id = azurerm_mssql_server.mssql.id
    is_manual_connection           = "false"
    subresource_names              = ["sqlServer"]
  }

  private_dns_zone_group {
    name                 = "privateDnsZones"
    private_dns_zone_ids = var.privateDnsZones
  }
}

resource "azurerm_mssql_virtual_network_rule" "vnet_rule" {
  count     = length(var.subnetId) > 0 ? 1 : 0
  name      = "sql-vnet-rule"
  server_id = azurerm_mssql_server.mssql.id
  subnet_id = var.subnetId
}
