# Unreleased
## Added
## Changed
## Fixed

# [0.2.3] - 2023-08-14
## Added
- Add azuread_administrator

# [0.2.2] - 2023-04-06
## Added
- Add public_network_access

# [0.1.1] - 2023-01-02
## Added
- Add tags

# [0.1.0] - 2022-11-11
## Added
- init
## Changed
## Fixed