variable "name" {
  type = string
}

variable "location" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "mssql_version" {
  type = string
}

variable "max_size_gb" {
  type = number
}

variable "sku" {
  type = object({
    name     = string
    tier     = string
    family   = string
    capacity = number
  })
}

variable "per_database" {
  type = object({
    min_capacity = number
    max_capacity = number
  })
}

variable "subnetId" {
  type = string
}
variable "privateDnsZones" {
  type = list(string)
}

variable "public_network_access" {
  type    = string
  default = "false"
}

variable "azuread_administrator" {
  type = object({
    login_username = string
    object_id      = string
  })
  default = null
}
